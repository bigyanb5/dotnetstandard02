﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test17022020;

namespace UnitTest1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            test1702 ts = new test1702();
            string s = ts.getData("str");
            Assert.AreEqual("Hello :str", s);
        }
    }
}
